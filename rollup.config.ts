import typescript from "@rollup/plugin-typescript";
import { defineConfig } from "rollup";
import { terser } from "rollup-terser";

export default defineConfig({
  input: "src/index.ts",
  output: {
    dir: "./dist",
    format: "cjs",
  },
  plugins: [
    typescript({
      tsconfig: "./tsconfig.json",
      module: "esnext",
    }),
    terser({}),
  ],
  external: [
    "express",
    "jsonwebtoken",
    "mongoose",
    "http",
    "dotenv/config",
    "express-async-handler",
    "http-status-codes",
    "bcrypt",
    "morgan",
    "cors"
  ],
});
