import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";

import APP from "../config/app";

const generateSecurePassword = (data: string | Buffer, salt = APP.SALT!) =>
  bcrypt.hashSync(data, salt);

const comparePassword = (data: string | Buffer, encrypted: string) =>
  bcrypt.compareSync(data, encrypted);

const generateJWT = (payload: string | object | Buffer) =>
  jwt.sign(payload, APP.JWT_SECRET, { expiresIn: "1d" });
const verifyJWT = (token: string) => jwt.verify(token, APP.JWT_SECRET);
const decodeJWT = (token: string) => jwt.decode(token);

export {
  generateSecurePassword,
  comparePassword,
  generateJWT,
  verifyJWT,
  decodeJWT,
};
