import mongoose from "mongoose";

import APP from "../config/app";

const mongoconnect = async () => {
  try {
    const mongo = await mongoose.connect(APP.DATABASE_URL!, {
      connectTimeoutMS: 100000
    });
    console.log("MongoDB connected.");

    return mongo;
  } catch (err) {
    throw err;
  }
};

export default mongoconnect;
