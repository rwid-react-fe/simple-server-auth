import http from "http";
import express from "express";

import Router from "../router";
import Middleware from "../middleware";
import ErrorHandler from "../execption/ErrorHandler";

const createServer = () => {
  const application = express();

  application.set("trust proxy", 1);

  Middleware.use(application);
  Router.use(application);
  ErrorHandler.use(application);

  return new http.Server(application);
};

export default createServer;
