import { StatusCodes } from "http-status-codes";

export default class ExtendedError extends Error {
  code: number;

  constructor() {
    super();

    this.code = StatusCodes.INTERNAL_SERVER_ERROR;
  }
}
