import { ReasonPhrases, StatusCodes } from "http-status-codes";
import ExtendedError from "./Error";

export default class AuthorizationExecption extends ExtendedError {
  message: string;
  name: string;
  code: number;

  constructor() {
    super();
    
    this.message = ReasonPhrases.UNAUTHORIZED;
    this.name = ReasonPhrases.UNAUTHORIZED;
    this.code = StatusCodes.UNAUTHORIZED;
  }
}
