import { Express, NextFunction, Request, Response } from "express";
import ExtendedError from "./Error";
import { JsonWebTokenError } from "jsonwebtoken";
import { StatusCodes } from "http-status-codes";

export default class ErrorHandler {
  static use(application: Express) {
    application.use(this._handler);
  }

  private static _handler(
    err: any,
    req: Request,
    res: Response,
    next: NextFunction,
  ) {
    if (err instanceof ExtendedError) {
      return res.status(err.code).json({
        error: err,
      });
    }

    if (err instanceof JsonWebTokenError) {
      return res.status(StatusCodes.UNAUTHORIZED).json({
        error: err,
      });
    }

    // console.log({ err: err.message });

    return res.status(500).json({
      error: err,
    });
  }
}
