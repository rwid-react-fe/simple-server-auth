import { faker } from "@faker-js/faker";

import { generateSecurePassword } from "../lib/auth.lib";

interface IUserFactory {
  create(user?: Partial<IUser>): Omit<IUser, "auth">;
}

export default class UserFactory implements IUserFactory {
  create(user?: Partial<IUser>) {
    return {
      username: faker.internet.userName(),
      email: faker.internet.email(),
      password: generateSecurePassword(faker.internet.password()),
      ...user,
    };
  }
}
