import { Router } from "express";
import expressAsyncHandler from "express-async-handler";

import AuthController from "../controller/auth.controller";
import authMiddleware from "../middleware/auth.middleware";

const router = Router();

class AuthRouter {
  private static _router = router;

  private static _setup() {
    this._router.post("/login", expressAsyncHandler(AuthController.login));
    this._router.get(
      "/verify-login",
      authMiddleware,
      expressAsyncHandler(AuthController.verfiyLogin),
    );

    return this._router;
  }

  static router() {
    return this._setup();
  }
}

export default AuthRouter;
