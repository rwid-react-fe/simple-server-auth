import { Express } from "express";

import AuthRouter from "./auth";

const routers = [AuthRouter.router()];

export default class Router {
  static use(application: Express) {
    application.use("/v1", routers);
  }
}
