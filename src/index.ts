import "dotenv/config";
import APP from "./config/app";

import createServer from "./lib/createServer";
import mongoconnect from "./lib/db";

createServer().listen(APP.PORT, async () => {
  await mongoconnect();
  console.log(`Server running on port : ${APP.PORT}`);
});
