const APP = {
  DATABASE_URL: process.env.DATABASE_URL,
  PORT: process.env.PORT || 8000,
  SALT: +process.env.SALT! || 10,
  JWT_SECRET: process.env.JWT_SECRET || "please give a secret!",
};

export default APP;
