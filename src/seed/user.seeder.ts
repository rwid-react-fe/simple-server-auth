import { generateSecurePassword } from "../lib/auth.lib";

import UserFactory from "../factory/user.factory";

import UserModel from "../model/User.model";

export default class UserSeeder {
  async run(amount = 5) {
    await UserModel.deleteMany({});

    return Promise.all(
      [...Array(amount)].map(async (_, i) => {
        if (i != 0) {
          return UserModel.create(
            new UserFactory().create({
              password: generateSecurePassword("user"),
            }),
          );
        }

        return UserModel.create(
          new UserFactory().create({
            username: "developer_123",
            email: "developer@dev.id",
            password: generateSecurePassword("developer"),
          }),
        );
      }),
    );
  }
}
