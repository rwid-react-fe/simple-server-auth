import "dotenv/config";
import mongoose from "mongoose";

import UserSeeder from "./user.seeder";

import mongoconnect from "../lib/db";

const seeders = [new UserSeeder().run];

const run = async () => {
  let mongo: typeof mongoose;
  try {
    mongo = await mongoconnect();

    for (let seeder of seeders) {
      await seeder();
    }
  } catch (err) {
    throw err;
  } finally {
    mongo!.connection.close();
    console.log("Finish seeding");
  }
};

run();
