import { StatusCodes, ReasonPhrases } from "http-status-codes";

import UserModel from "../model/User.model";

import { LoginRequestBody } from "../controller/auth.controller";
import { comparePassword, generateJWT } from "../lib/auth.lib";

export default class AuthService {
  static async login(body: LoginRequestBody) {
    const { email, password } = body.data;

    try {
      const user = await UserModel.findOne({ email }).select({
        token: 0,
      });

      if (user) {
        const isPasswordCorrect = comparePassword(password, user.password);

        if (isPasswordCorrect) {
          const token = generateJWT({ user });

          await UserModel.findOneAndUpdate(
            { _id: user.id },
            {
              $set: {
                token,
              },
            },
          );

          return { user, token, status: StatusCodes.OK };
        }
      }
      return {
        message: ReasonPhrases.UNAUTHORIZED,
        status: StatusCodes.UNAUTHORIZED,
      };
    } catch (err) {
      throw err;
    }
  }
}
