import { NextFunction, Request, Response } from "express";

import { JwtPayload } from "jsonwebtoken";

import AuthorizationExecption from "../execption/AuthorizationExecption";
import { decodeJWT, verifyJWT } from "../lib/auth.lib";

const authMiddleware = (req: Request, res: Response, next: NextFunction) => {
  const token = req.headers.authorization?.split(" ")[1];

  if (token) {
    const isTokenExp = verifyJWT(token);

    if (!isTokenExp) {
      throw new AuthorizationExecption();
    }

    const decoded = decodeJWT(token) as JwtPayload;
    req.users = decoded.user;

    return next();
  }

  throw new AuthorizationExecption();
};

export default authMiddleware;
