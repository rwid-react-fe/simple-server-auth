import express, { Express } from "express";
import morgan from "morgan";
import cors from "cors";

export default class Middleware {
  static use(application: Express) {
    application.use(
      cors({
        origin: "*",
      }),
    );
    application.use(express.json());
    application.use(morgan("dev"));
  }
}
