import { NextFunction, Request, Response } from "express";
import { ParamsDictionary } from "express-serve-static-core";
import AuthService from "../service/Auth.service";
import { StatusCodes } from "http-status-codes";

export interface LoginRequestBody {
  data: {
    email: string;
    password: string;
  };
}

interface LoginRequest
  extends Request<ParamsDictionary, any, LoginRequestBody> {}

export default class AuthController {
  static async login(req: LoginRequest, res: Response, next: NextFunction) {
    const { status, ...data } = await AuthService.login(req.body);

    res.status(status).json({
      data,
    });
  }

  static async verfiyLogin(
    req: LoginRequest,
    res: Response,
    next: NextFunction,
  ) {
    res.status(StatusCodes.OK).json({
      data: req.users,
    });
  }
}
