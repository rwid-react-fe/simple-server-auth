import { Schema, model } from "mongoose";

const UserSchema = new Schema<IUser>(
  {
    username: String,
    password: String,
    email: String,
    token: String
  },
  { timestamps: true },
);

const UserModel = model<IUser>("users", UserSchema);

export default UserModel;
