# Getting Started

## How to use

Clone this repo to your local machine.

```
git clone git@gitlab.com:rwid-react-fe/simple-server-auth.git
```

Install using package manger

```
yarn install
```

Setup `.env` files then seed your database use `yarn seed` commands.

`yarn dev` to running server development and `yarn build` to build the projects

## **Routes**

* **POST** /v1/login to login
* **GET** /v1/verify-login to check login status
