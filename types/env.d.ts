declare namespace NodeJS {
  type LocalProcessEnv = Partial<{
    DATABASE_URL: string;
    PORT: number;
    SALT: string | number;
    JWT_SECRET: string;
  }>;

  interface ProcessEnv extends LocalProcessEnv {}
}
