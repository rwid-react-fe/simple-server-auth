declare global {
  interface IUser {
    username: string;
    password: string;
    email: string;
    token?: string;
  }
}

export {};
