declare module "jsonwebtoken" {
  export interface JwtPayload {
    user: IUser & {
      createdAt: string;
      updatedAt: string;
      __v: number;
    };
  }
}

export {};
